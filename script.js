document.addEventListener('scroll', function () {
    const fixedDiv = document.querySelector('.outer');
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;

    if (scrollTop > 100) {
        fixedDiv.style.transform = 'translateY(' + scrollTop + 'px)';
    } else {
        fixedDiv.style.transform = 'translateY(0)';
    }
});