AUGMENTED ROBOTICS - WEB DEVELOPMENT CHALLENGE:

HOW TO RUN THE CODE:
1. Create the folder "RESOURCES" in the folder holding the HTML, CSS and JavaScript code files (unless created already).
2. Populate the folder with all the necessary resources (unless populated already):
	2.1: Background.jpg
	2.2: ARWebsiteBG.jpg
	2.3: Augmented Robotics Logo Vertical White.svg
	2.4: MobilePhone.png
	2.5: HeaderVideo_FullColor.mp4
	2.6: DotLine.svg
	2.7: controller.png
	2.8: PhoneCity.png
	2.9: car.png
	2.10: RobotWaving.png
3. Copy the path of the folder hosting the HTML file and paste it into a browser, including the name of the HTML file at the end.
	e.g. C:\Users\Bob\source\repos\AugmentedRobotics\index.html

PROGRAM IMPLEMENTATION:

The program is split into multiple sections, with various subsections.

1. title-page:
	- Contains background image, and is positioned at the top.
	- Provides a backdrop for the slogan, links and the phone animation (initially).

2. slogan:
	- "EXPAND YOUR REALITY", uses "DotLine.svg" as a divider.

3. header:
	- Located at the very top of the title page, this class hosts the links to:
		> Services
		> Projects
		> Company
	- It also holds the company logo. I was unfortunately unable to find a horizontal one in correct format and colour.

4. outer:
	- The outer boundary to the "phone-video" class, holding the phone frame and the video.
	- Video is put first, the phone frame overlays it ("frame-overlay").
	- "script.js" handles the triggering of animation upon scrolling down.

5. content:
	- First are a header and its subtext, broadly outlining what Augmented Robotics have to offer.
	- "row" class holds 3 "columns", each stacking text, image and a button.
		> Columns are then stacked next to each other, as opposed to on one another.

6. footer:
	- This section holds an invitation to get in touch with Augmented Robotics.
	- I've changed the colour scheme of the button to make it stand out from the rest.
